#!/usr/bin/env

import argparse
import re
import pygraphviz as PG
from urllib.parse import urlparse

#### Argument parsing
arg_parser = argparse.ArgumentParser()

arg_parser.add_argument('-i', '--dirbFile', type=str, help="The dirb results file location", required=True)
args = arg_parser.parse_args()
#### 


DIRB_FILE = args.dirbFile
OUT_FILE = args.dirbFile

graph = PG.AGraph(directed=True, strict=True)


with open(DIRB_FILE, 'r') as f:
    content = f.readlines()


folders = []
files = {}
url_base = ""

for line in content:
    if not line.find('URL_BASE: '):
        url_base = line[10:-1]
        folders.append(line[10:-1])
    if not line.find('==> DIRECTORY:'):
        folders.append(line[15:-1])
    if not line.find('+ '):
        if line.split(' ')[1][-1:] != '/':
            files[line.split(' ')[1]] = line.split(' ')[2][:-1]
        else:
            folders.append(line.split(' ')[1])

# Remove duplicates
folders = list(set(folders))
folders.sort()


# Print tree to stdour
print('####################################################################')
print('########## '+url_base)
print('####################################################################')
for folder in folders:
    # Path is a list of URL sub /<element>/
    path = urlparse(folder).path.split('/')
    path = list(filter(None, path))
    if path: 
        for i in range(0, len(path)):
            if i == 0:
                if graph.has_node(str(path[0])) is False:
                    graph.add_node(str(path[0]))
                if graph.has_edge("/", path[0]) is False:
                    graph.add_edge("/", path[0])
            else: 
                child_node = ''
                parent_node = ''
                for j in range(i):
                    child_node += path[j]
                    parent_node += path[j]
                child_node += path[i]
                if graph.has_node(child_node) is False:
                    graph.add_node(child_node, label=path[i])
                    graph.add_edge(parent_node, child_node)

    for f in files:
        filename = f.split("/")[-1]
        base = f.replace(filename, '')
        
        path = urlparse(f).path.split('/')
        path = list(filter(None, path))
        
        child_node = ''
        parent_node = ''
        for j in range(len(path)-1):
            child_node += path[j]
            parent_node += path[j]
        child_node += path[len(path)-1]
        if graph.has_node(parent_node) is True:
            graph.add_node(child_node, label=filename, color='#eeeeee', style='filled')
            graph.add_edge(parent_node, child_node)


graph.write('graph.dot')
graph.layout(prog='dot')





